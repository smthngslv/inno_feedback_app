"""empty message

Revision ID: 1503947cff35
Revises: 
Create Date: 2020-11-22 15:25:27.354360

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1503947cff35'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('course',
    sa.Column('_id', sa.Integer(), nullable=False),
    sa.Column('_name', sa.String(length=64), nullable=False),
    sa.PrimaryKeyConstraint('_id')
    )
    op.create_table('user',
    sa.Column('_id', sa.Integer(), nullable=False),
    sa.Column('_email', sa.String(), nullable=False),
    sa.PrimaryKeyConstraint('_id'),
    sa.UniqueConstraint('_email')
    )
    op.create_table('form',
    sa.Column('_id', sa.Integer(), nullable=False),
    sa.Column('_course_id', sa.Integer(), nullable=False),
    sa.Column('_name', sa.String(length=64), nullable=False),
    sa.Column('_post_time', sa.DateTime(), nullable=False),
    sa.Column('_deadline', sa.DateTime(), nullable=False),
    sa.Column('_data', sa.LargeBinary(), nullable=False),
    sa.ForeignKeyConstraint(['_course_id'], ['course._id'], ),
    sa.PrimaryKeyConstraint('_id')
    )
    op.create_table('user_to_course_map',
    sa.Column('_user_id', sa.Integer(), nullable=False),
    sa.Column('_course_id', sa.Integer(), nullable=False),
    sa.Column('_permission', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['_course_id'], ['course._id'], ),
    sa.ForeignKeyConstraint(['_user_id'], ['user._id'], ),
    sa.PrimaryKeyConstraint('_user_id', '_course_id')
    )
    op.create_table('feedback',
    sa.Column('_id', sa.Integer(), nullable=False),
    sa.Column('_form_id', sa.Integer(), nullable=False),
    sa.Column('_data', sa.LargeBinary(), nullable=False),
    sa.ForeignKeyConstraint(['_form_id'], ['form._id'], ),
    sa.PrimaryKeyConstraint('_id')
    )
    op.create_table('filled_form_map',
    sa.Column('_user_id', sa.Integer(), nullable=False),
    sa.Column('_form_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['_form_id'], ['form._id'], ),
    sa.ForeignKeyConstraint(['_user_id'], ['user._id'], ),
    sa.PrimaryKeyConstraint('_user_id', '_form_id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('filled_form_map')
    op.drop_table('feedback')
    op.drop_table('user_to_course_map')
    op.drop_table('form')
    op.drop_table('user')
    op.drop_table('course')
    # ### end Alembic commands ###
