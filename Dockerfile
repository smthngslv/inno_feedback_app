FROM python:3.8

WORKDIR /app

COPY requirements.txt requirements.txt

RUN python -m venv venv
RUN . venv/bin/activate
RUN pip install -r requirements.txt

COPY app app
COPY migrations migrations
COPY .env.example .env
COPY inno_feedback_app.py config.py boot.sh app.db ./

RUN chmod +x boot.sh

EXPOSE 5000
CMD ["./boot.sh"]
