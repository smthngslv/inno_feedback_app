from app           import mail, app
from flask         import url_for, redirect, request
from flask_mail    import Message
from werkzeug.urls import url_parse
from threading     import Thread


def redirect_next(default = 'index'):
    next_page = request.args.get('next')
    
    if next_page and url_parse(next_page).netloc:
        return redirect(next_page)
    
    return redirect(url_for(default))

def get_element_by_id(model, name = 'id'):
    id = request.args.get(name)
    
    if id and id.isdigit():
        return model.query.get(int(id))
    
    return None

def send_email_async(app, msg):
    with app.app_context():
        mail.send(msg)

def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender = sender, recipients = recipients)
    
    msg.body = text_body
    msg.html = html_body
    
    Thread(target = send_email_async, args = (app, msg)).start()
