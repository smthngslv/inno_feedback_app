/*
    ----------------------> Configuration section <------------------------
    There are default setting for graphs. If you want customize all of them
    you can change these. However, if you need to customize specific chart
    you need to do the following:
    1. All these settings have common part "Chart.defaults.global"
    2. Create dictionary called options
    3. Set all settings you want by treating part "options." as "Chart.defaults.global."
    4. Pass the options into the chart creating function.
*/

// Responsiveness
Chart.defaults.global.responsive = true; // Resizes the chart canvas when its container does
Chart.defaults.global.responsiveAnimationDuration = 100; // Duration in milliseconds it takes to animate to new size after a resize event.
Chart.defaults.global.maintainAspectRatio = false; // Maintain the original canvas aspect ratio (width / height) when resizing.
Chart.defaults.global.aspectRatio = 1; /* Canvas aspect ratio (i.e. width / height, a value of 1 representing a square canvas). 
Note that this option is ignored if the height is explicitly defined either as attribute or via the style. */

// Interactions
Chart.defaults.global.hover.mode = "nearest"; // Sets which elements appear in the tooltip.
Chart.defaults.global.hover.intersect = true; // If true, the hover mode only applies when the mouse position intersects an item on the chart.
Chart.defaults.global.hover.axis = "x"; /* Can be set to "x", "y", or "xy" to define which directions are used in calculating distances.
Defaults to "x" for "index" mode and "xy" in dataset and "nearest" modes. */
Chart.defaults.global.hover.animationDuration = 100; // Duration in milliseconds it takes to animate hover style changes.

// Fonts
Chart.defaults.global.defaultFontColor = "#777777"; // Default font color for all text.
Chart.defaults.global.defaultFontFamily = "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif"; // Default font family for all text.
Chart.defaults.global.defaultFontSize = 15; // Default font size (in px) for text. Does not apply to radialLinear scale point labels.
Chart.defaults.global.defaultFontStyle = "normal"; // Default font style. Does not apply to tooltip title or footer. Does not apply to chart title.

// Animations
Chart.defaults.global.animation.duration = 1200; // The number of milliseconds an animation takes.
Chart.defaults.global.animation.easing = "easeOutQuad"; // Easing function to use. See https://www.chartjs.org/docs/latest/configuration/animations.html#easing
// TODO : ↑ choose nice animation ↑

// Layout
Chart.defaults.global.layout.padding = { left: 0, right: 0, top: 0, bottom: 0 }; // The padding to add inside the chart.

// Legend
Chart.defaults.global.legend.display = false; // Is the legend shown?
Chart.defaults.global.legend.position = "bottom"; // Position of the legend.
Chart.defaults.global.legend.align = "center"; // Alignment of the legend.
Chart.defaults.global.legend.fullWidth = true; // Marks that this box should take the full width of the canvas (pushing down other boxes).
Chart.defaults.global.legend.reverse = false; // Legend will show datasets in reverse order.
Chart.defaults.global.legend.rtl = false; // true for rendering the legends from right to left.
Chart.defaults.global.legend.labels.boxWidth = 12; // Width of coloured box.
Chart.defaults.global.legend.labels.fontSize = 15; // Font size of text.
Chart.defaults.global.legend.labels.fontStyle = "normal"; // Font style of text.
Chart.defaults.global.legend.labels.fontColor = "#777777"; // Color of text.
Chart.defaults.global.legend.labels.fontFamily = "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif"; // Font family of legend text.
Chart.defaults.global.legend.labels.padding = 10; // Padding between rows of colored boxes.

// Title
Chart.defaults.global.title.display = false; // Is the title shown?
Chart.defaults.global.title.position = "top"; // Position of title.
Chart.defaults.global.title.fontSize = 18; // Font size.
Chart.defaults.global.title.fontFamily = "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif"; // Font family.
Chart.defaults.global.title.fontColor = "#777777"; // Font color.
Chart.defaults.global.title.fontStyle = "bold"; // Font style.
Chart.defaults.global.title.padding = 10; // Number of pixels to add above and below the title text.
Chart.defaults.global.title.lineHeight = 1.2; // Height of an individual line of text.
Chart.defaults.global.title.text = "Statistics Chart"; // Title text to display. If specified as an array, text is rendered on multiple lines.

// Tooltips
Chart.defaults.global.tooltips.enabled = true; // Are on-canvas tooltips enabled?
Chart.defaults.global.tooltips.custom = null; // See custom tooltip section.
Chart.defaults.global.tooltips.mode = "nearest"; // Sets which elements appear in the tooltip.
Chart.defaults.global.tooltips.intersect = true; // If true, the tooltip mode applies only when the mouse position intersects with an element. If false, the mode will be applied at all times.
Chart.defaults.global.tooltips.position = "average"; // The mode for positioning the tooltip. more...
Chart.defaults.global.tooltips.backgroundColor = "#555555dd"; // Background color of the tooltip.
Chart.defaults.global.tooltips.titleFontFamily = "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif"; // Title font.
Chart.defaults.global.tooltips.titleFontSize = 15; // Title font size.
Chart.defaults.global.tooltips.titleFontStyle = "normal"; // Title font style.
Chart.defaults.global.tooltips.titleFontColor = "white"; // Title font color.
Chart.defaults.global.tooltips.titleAlign = "left"; // Horizontal alignment of the title text lines. more...
Chart.defaults.global.tooltips.titleSpacing = 2; // Spacing to add to top and bottom of each title line.
Chart.defaults.global.tooltips.titleMarginBottom = 6; // Margin to add on bottom of title section.
Chart.defaults.global.tooltips.bodyFontFamily = "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif"; // Body line font.
Chart.defaults.global.tooltips.bodyFontSize = 14; // Body font size.
Chart.defaults.global.tooltips.bodyFontStyle = "normal"; // Body font style.
Chart.defaults.global.tooltips.bodyFontColor = "white"; // Body font color.
Chart.defaults.global.tooltips.bodyAlign = "left"; // Horizontal alignment of the body text lines. more...
Chart.defaults.global.tooltips.bodySpacing = 2; // Spacing to add to top and bottom of each tooltip item.
Chart.defaults.global.tooltips.footerFontFamily = "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif"; // Footer font.
Chart.defaults.global.tooltips.footerFontSize = 14; // Footer font size.
Chart.defaults.global.tooltips.footerFontStyle = "bold"; // Footer font style.
Chart.defaults.global.tooltips.footerFontColor = "white"; // Footer font color.
Chart.defaults.global.tooltips.footerAlign = "left"; // Horizontal alignment of the footer text lines. more...
Chart.defaults.global.tooltips.footerSpacing = 2; // Spacing to add to top and bottom of each footer line.
Chart.defaults.global.tooltips.footerMarginTop = 6; // Margin to add before drawing the footer.
Chart.defaults.global.tooltips.xPadding = 6; // Padding to add on left and right of tooltip.
Chart.defaults.global.tooltips.yPadding = 6; // Padding to add on top and bottom of tooltip.
Chart.defaults.global.tooltips.caretPadding = 2; // Extra distance to move the end of the tooltip arrow away from the tooltip point.
Chart.defaults.global.tooltips.caretSize = 5; // Size, in px, of the tooltip arrow.
Chart.defaults.global.tooltips.cornerRadius = 0; // Radius of tooltip corner curves.
Chart.defaults.global.tooltips.multiKeyBackground = "#fff"; // Color to draw behind the colored boxes when multiple items are in the tooltip.
Chart.defaults.global.tooltips.displayColors = true; // If true, color boxes are shown in the tooltip.
Chart.defaults.global.tooltips.borderColor = "rgba(0, 0, 0, 0)"; // Color of the border.
Chart.defaults.global.tooltips.borderWidth = 0; // Size of the border.
Chart.defaults.global.tooltips.rtl = false; // for rendering the legends from right to left.

// Elements
Chart.defaults.global.elements.point.radius = 3; // Point radius.
Chart.defaults.global.elements.point.pointStyle = "circle"; // Point style.
Chart.defaults.global.elements.point.rotation = 0; // Point rotation (in degrees).
Chart.defaults.global.elements.point.backgroundColor = "rgba(0, 0, 0, 0.1)"; // Point fill color.
Chart.defaults.global.elements.point.borderWidth = 1; // Point stroke width.
Chart.defaults.global.elements.point.borderColor = "rgba(0, 0, 0, 0.1)"; // Point stroke color.
Chart.defaults.global.elements.point.hitRadius = 1; // Extra radius added to point radius for hit detection.
Chart.defaults.global.elements.point.hoverRadius = 4; // Point radius when hovered.
Chart.defaults.global.elements.point.hoverBorderWidth = 1; // Stroke width when hovered.

Chart.defaults.global.elements.line.tension = 0.4; // Bezier curve tension (0 for no Bezier curves).
Chart.defaults.global.elements.line.backgroundColor = "rgba(0, 0, 0, 0.1)"; // Line fill color.
Chart.defaults.global.elements.line.borderWidth = 3; // Line stroke width.
Chart.defaults.global.elements.line.borderColor = "rgba(0, 0, 0, 0.1)"; // Line stroke color.
Chart.defaults.global.elements.line.borderCapStyle = "butt"; // Line cap style.
Chart.defaults.global.elements.line.borderDash = []; // Line dash.
Chart.defaults.global.elements.line.borderDashOffset = 0.0; // Line dash offset.
Chart.defaults.global.elements.line.borderJoinStyle = "miter"; // Line join style.
Chart.defaults.global.elements.line.capBezierPoints = true; // true to keep Bezier control inside the chart, false for no restriction.
Chart.defaults.global.elements.line.cubicInterpolationMode = "default"; // Interpolation mode to apply.
Chart.defaults.global.elements.line.fill = true; // How to fill the area under the line. See area charts.
Chart.defaults.global.elements.line.stepped = false; // true to show the line as a stepped line (tension will be ignored).

Chart.defaults.global.elements.rectangle.backgroundColor = "rgba(0, 0, 0, 0.1)"; // Bar fill color.
Chart.defaults.global.elements.rectangle.borderWidth = 0; // Bar stroke width.
Chart.defaults.global.elements.rectangle.borderColor = "rgba(0, 0, 0, 0.1)"; // Bar stroke color.
Chart.defaults.global.elements.rectangle.borderSkipped = "bottom"; // Skipped (excluded) border: "bottom", "left", "top" or "right".

// Chart.defaults.global.elements.arc.angle = circumference / (arc count); // Arc angle to cover. // idk what is that
Chart.defaults.global.elements.arc.backgroundColor = "rgba(0, 0, 0, 0.1)"; // Arc fill color.
Chart.defaults.global.elements.arc.borderAlign = "center"; // Arc stroke alignment.
Chart.defaults.global.elements.arc.borderColor = "#fff"; // Arc stroke color.
Chart.defaults.global.elements.arc.borderWidth = 2; // Arc stroke width.

function createLineChart(canvas_name, labels, values, limit) {

    var datasets = [
        {
            data: values,

            // Please mind that the following settings override the defaults.
            //backgroundColor: "#fa823177",
            borderCapStyle: "butt",
            borderColor: "#20bf6b",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            borderWidth: 3,
            clip: 3 / 2, // (borderWidth) / 2
            cubicInterpolationMode: "default",
            fill: false,
            hoverBackgroundColor: undefined,
            hoverBorderCapStyle: undefined,
            hoverBorderColor: undefined,
            hoverBorderDash: undefined,
            hoverBorderDashOffset: undefined,
            hoverBorderJoinStyle: undefined,
            hoverBorderWidth: undefined,
            //label: "",
            lineTension: 0.3,
            order: 0,
            pointBackgroundColor: "#26de81",
            pointBorderColor: "#4b6584cc",
            pointBorderWidth: 2,
            pointHitRadius: 2,
            pointHoverBackgroundColor: undefined,
            pointHoverBorderColor: undefined,
            pointHoverBorderWidth: 1,
            pointHoverRadius: 4,
            pointRadius: 4,
            pointRotation: 0,
            pointStyle: "circle",
            showLine: true,
            spanGaps: undefined,
            steppedLine: false,
        }
    ];

    var chartContext = document.getElementById(canvas_name).getContext("2d");

    var chartData = {
        labels: labels,
        datasets: datasets
    };

    var LineChart = new Chart(chartContext, {
        type: "line",
        data: chartData
    });
}

function createPieChart(canvas_name, labels, values, limit) {

    var datasets = [
        {
            data: values,

            // Please mind that the following settings override the defaults.
            
            // The following properties can be customized per each arc. For that use arrays.
            // < I added 10 colors, so chart support up to 10 pie pieces. :) >
            backgroundColor: ["#20bf6b", "#0fb9b1", "#2d98da", "#3867d6", "#8854d0",
                              "#eb3b5a", "#fa8231", "#f7b731", "#a5b1c2", "#4b6584"],
            borderAlign: "center",
            borderColor: "rgba(255, 255, 255, 0.5)",
            borderWidth: 1,
            hoverBackgroundColor: undefined,
            hoverBorderColor: "rgba(255, 255, 255, 1)",
            hoverBorderWidth: 3,
            weight: 1
        }
    ];

    var chartContext = document.getElementById(canvas_name).getContext("2d");

    var chartData = {
        labels: labels,
        datasets: datasets
    };

    // < I decided that pie chart requires a legend despite defaults :) >
    var options = {
        legend: {
            display: true,
        }
    }

    var myPieChart = new Chart(chartContext, {
        type: "pie",
        data: chartData,
        options: options
    });
}