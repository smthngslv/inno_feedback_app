from app              import db
from app.models.utils import Permission

class UserToCourseMap(db.Model):
    _user_id    = db.Column(db.Integer, db.ForeignKey('user._id'),   nullable = False,  primary_key = True)
    _course_id  = db.Column(db.Integer, db.ForeignKey('course._id'), nullable = False, primary_key = True)
    _permission = db.Column(db.Integer, nullable = False, default = Permission.NONE)
    
    _user   = db.relationship('User',   back_populates = '_user_to_courses_map')
    _course = db.relationship('Course', back_populates = '_course_to_users_map') 
    
    @property
    def user(self):
        return self._user
    
    @property
    def course(self):
        return self._course
     
    @property
    def permission(self):
        return self._permission
