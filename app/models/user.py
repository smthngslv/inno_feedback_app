import jwt
import time

from flask_login import UserMixin

from app        import db, login, app
from app.models import UserToCourseMap


class User(UserMixin, db.Model):
    _id    = db.Column(db.Integer, nullable = False, primary_key = True)
    _email = db.Column(db.String,  nullable = False, unique = True)
    
    _filled_forms        = db.relationship('FilledFormMap',   back_populates = '_user')
    _user_to_courses_map = db.relationship('UserToCourseMap', back_populates = '_user')
    
    @staticmethod
    def get_by_email(email):
        return User.query.filter(User._email == email).first()
    
    @staticmethod
    def get_by_token(token):
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'], algorithms = ['HS256'])['auth']
        except:
            return None
        
        return User.query.get(id)
    
    @staticmethod
    @login.user_loader
    def load(id):
        return User.query.get(int(id))
    
    @property
    def id(self):
        return self._id
    
    @property
    def email(self):
        return self._email
    
    @property
    def courses(self):
        return [association.course for association in self._user_to_courses_map]        
    
    def get_auth_token(self, expires_in = 600):
        return jwt.encode({'auth': self._id, 'exp': time.time() + expires_in }, app.config['SECRET_KEY'], algorithm='HS256').decode()
    
    def add_course(self, course, permission):
        association = UserToCourseMap(_user_id    = self._id, 
                                      _course_id  = course._id, 
                                      _permission = permission)
        
        self._user_to_courses_map.append(association)

    def __repr__(self):
        return f'< User #{self.id} >'

