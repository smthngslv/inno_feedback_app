import json

from datetime import datetime
from datetime import timezone

from collections import Counter

from app import db
from app.models       import FilledFormMap, Feedback

# Example of the data (before encoding())
#questions = [{'type': 'StringField', 'name': 'question_0', 'label': 'Вопрос_1', 'validators':[{'type':'DataRequired'}]},
#             {'type':  'RadioField', 'name': 'label': 'Вопрос_2' 'question_1', 'params':{'choices':['Yes', 'No']}}]

class Form(db.Model):
    _id        = db.Column(db.Integer,     nullable = False, primary_key = True)
    _course_id = db.Column(db.Integer,    db.ForeignKey('course._id'), nullable = False)
    _name      = db.Column(db.String(64),  nullable = False)
    _post_time = db.Column(db.DateTime,    nullable = False, default = datetime.utcnow)
    _deadline  = db.Column(db.DateTime,    nullable = False, default = datetime.max)
    _data      = db.Column(db.LargeBinary, nullable = False, default = b'[]')
    
    _course       = db.relationship('Course',        back_populates = '_forms')
    _feedbacks    = db.relationship('Feedback',      back_populates = '_form')
    _filled_users = db.relationship('FilledFormMap', back_populates = '_form')
    
    @property
    def id(self):
        return self._id
    
    @property
    def name(self):
        return self._name
    
    @property
    def course(self):
        return self._course
        
    @property
    def feedbacks(self):
        return self._feedbacks
        
    @property
    def questions(self):
        return json.loads(self._data.decode())
    
    @property
    def deadline(self):
        return self._deadline
    
    @property
    def statistics(self):
        questions = self.questions
        feedbacks = [feedback.answers for feedback in self._feedbacks]
        
        statistics = []
        
        for (question, i) in zip(questions, range(len(questions))):
            answers = [feedback[i] for feedback in feedbacks]
            
            label = question.get('params', {}).get('label', question['name'])
            
            graph = {'labels': [], 'values': []}
            
            for (answer, count) in Counter(answers).items():
                graph['labels'].append(answer)
                graph['values'].append(count)

            statistics.append((label, answers, graph))
            
        return statistics
    
    @questions.setter
    def questions(self, questions):
       self._data = json.dumps(questions).encode()
    
    def check_deadline(self, t = None):
        if not t:
            t = datetime.now()
             
        return self._deadline > t
    
    def check_permission(self, user, permisson):
        return self._course.check_permission(user, permisson)
    
    def is_filled_by(self, user):
        return FilledFormMap.is_filled(user, self)
    
    def leave_feedback(self, user, answers):
        db.session.add(FilledFormMap(_user_id = user.id, _form_id = self._id))
        db.session.add(Feedback(_form_id = self._id, answers = answers))
        db.session.commit()
    
    def remove(self):
        for mp in FilledFormMap.query.filter(FilledFormMap._form_id == self._id).all():
            db.session.delete(mp)
        
        for feedback in self._feedbacks:
            db.session.delete(feedback)
            
        db.session.delete(self)
        
        db.session.commit()
    
    def __repr__(self):
        return f'< Form "{self._name}" #{self._id} >'
     
 
