from app              import db
from app.models.utils import Permission

class FilledFormMap(db.Model):
    _user_id  = db.Column(db.Integer, db.ForeignKey('user._id'), nullable = False, primary_key = True)
    _form_id  = db.Column(db.Integer, db.ForeignKey('form._id'), nullable = False, primary_key = True)

    _user = db.relationship('User', back_populates = '_filled_forms')
    _form = db.relationship('Form', back_populates = '_filled_users') 
    
    @staticmethod
    def is_filled(user, form):
        filters = []
        
        filters.append(FilledFormMap._user_id == user.id)
        filters.append(FilledFormMap._form_id == form.id)
    
        return (FilledFormMap.query.filter(*filters).count() > 0)
    
    @property
    def user(self):
        return self._user
    
    @property
    def form(self):
        return self._form

    
     
