from enum import IntEnum


class Permission(IntEnum):
    # 0001
    #    ^ - Can see forms.
    # 0010
    #   ^  - Can send feedbacks.
    # 0100
    #  ^   - Can see feedbacks.
    # 1000
    # ^    - Can create forms.
    
    NONE = 0 #0000
    SPEC = 1 #0001
    FILL = 2 #0010
    VIEW = 4 #0100
    MAKE = 8 #1000
    
    STUDENT   =  3 #0011
    DOE       =  5 #0101
    PROFESSOR = 13 #1101
    
