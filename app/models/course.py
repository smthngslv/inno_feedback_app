from app              import db
from app.models       import Form, UserToCourseMap
from app.models.utils import Permission

class Course(db.Model):
    _id    = db.Column(db.Integer,    nullable = False, primary_key = True)
    _name  = db.Column(db.String(64), nullable = False)
    
    _forms               = db.relationship('Form',            back_populates = '_course', lazy = 'dynamic')
    _course_to_users_map = db.relationship('UserToCourseMap', back_populates = '_course')
    
    @property
    def id(self):
        return self._id
    
    @property
    def name(self):
        return self._name
    
    @property
    def users(self):
        return [association.user for association in self._course_to_users_map]
    
    def get_permission(self, user):
        filter_1 = UserToCourseMap._user_id   == user.id
        filter_2 = UserToCourseMap._course_id == self.id
        
        association = UserToCourseMap.query.filter(filter_1, filter_2).first()
        
        if association:
            return association.permission
    
        return None
    
    def check_permission(self, user, permission):
        filter_1 = UserToCourseMap._user_id   == user.id
        filter_2 = UserToCourseMap._course_id == self.id
        filter_3 = UserToCourseMap._permission.op('&')(permission) == permission
        
        return (UserToCourseMap.query.filter(filter_1, filter_2, filter_3).count() > 0)

    def get_forms_for_user(self, user, count = 5):
        forms = self._forms.order_by(Form._post_time.desc()).limit(count).all()
        
        for i in range(len(forms)):
            form = forms[i]
            
            permission = self.get_permission(user)
            
            can_fill = permission & Permission.FILL == Permission.FILL
            can_view = permission & Permission.VIEW == Permission.VIEW # or form.is_shared
            
            forms[i] = (form, can_fill, can_view)
            
        return forms
    
    def create_form(self, user, name, **kwargs):
        self._forms.append(Form(_name = name, **kwargs))
        
        db.session.commit()
    
    def __repr__(self):
        return f'< Course "{self.name}" #{self.id} >'
     
