import json

from app import db

class Feedback(db.Model):
    _id      = db.Column(db.Integer,     nullable = False, primary_key = True)
    _form_id = db.Column(db.Integer,    db.ForeignKey('form._id'), nullable = False)
    _data    = db.Column(db.LargeBinary, nullable = False, default = b'[]')
    
    _form = db.relationship('Form', back_populates = '_feedbacks')
    
    @property
    def id(self):
        return self._id
    
    @property
    def form(self):
        return self._form
    
    @property
    def answers(self):
        return json.loads(self._data.decode())
    
    @answers.setter
    def answers(self, questions):
       self._data = json.dumps(questions).encode()
    
    def check_permission(self, user, permisson):
        return self._form.check_permission(user, permisson)
    
    def get_feedback(self):
        feedback = []
        
        for (question, answer) in zip(self._form.questions, self.answers):
            feedback.append((question.get('params', {}).get('label', question['name']), answer))
            
        return feedback
    
    def __repr__(self):
        return f'< Feedback #{self.id} >'
     
 
