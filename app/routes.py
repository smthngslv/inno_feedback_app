import json

from flask       import url_for, redirect, request, render_template, flash
from flask_login import login_user, logout_user, login_required, current_user


import app.forms  as forms
import app.models as models

from app       import app, db
from app.utils import redirect_next, get_element_by_id, send_email

from math import ceil
import sys


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect_next()
    
    form = forms.Login()
    
    if form.validate_on_submit():
        user = models.User.get_by_email(form.email.data)
        
        if user is None:
            flash('No such user.')
            
            return redirect(url_for('login'))
        
        
        subject = 'Inno Feedback App Auth'
        
        sender  = app.config['MAIL_USERNAME']
        
        text_body = render_template('auth_email.txt', user = user)
        html_body = render_template('auth_email.html', user = user)
        
        send_email(subject, sender, [user.email, ], text_body, html_body)

        flash('An auth email sent.')
        
        return redirect(url_for('login'))
    
    return render_template('auth.html', form = form)

@app.route('/login_debug', methods=['GET', 'POST'])
def login_debug():
    if not app.config['DEBUG']:
        return redirect_next()
    
    if current_user.is_authenticated:
        return redirect_next()
    
    form = forms.LoginOld()
    
    if form.validate_on_submit():
        user = models.User.query.get(form.id.data)
        
        if user is None:
            flash('Invalid user_id.')
            
            return redirect(url_for('login'))
    
        login_user(user, remember = False)
        
        return redirect_next()
    
    return render_template('auth_old.html', form = form)

@app.route('/auth/<token>')
def auth(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    
    user = models.User.get_by_token(token)
    
    if not user:
        return redirect(url_for('index'))
    
    login_user(user, remember = False)

    return redirect(url_for('index'))

@app.route('/logout')
@login_required
def logout():
    logout_user()
    
    return redirect(url_for('login'))


@app.route('/')
@app.route('/courses')
@login_required
def index():    
    return render_template("index.html", title = 'Home Page')

@app.route('/courses/view')
@login_required
def view_course():
    course = get_element_by_id(models.Course)
    
    if course is None:
        return 'Cannot find the course.'
    
    if not course.check_permission(current_user, models.utils.Permission.SPEC):
        return 'No permissions.'
    
    return render_template('course_channel.html', course = course)


@app.route('/forms/fill', methods = ['GET', 'POST'])
@login_required
def fill_form():
    form = get_element_by_id(models.Form)
    
    if form is None:
        return 'Cannot find the form.'
    
    if not form.check_permission(current_user, models.utils.Permission.FILL):
        return 'No permission'
    
    if form.is_filled_by(current_user):
        return 'Already filled.'
    
    if not form.check_deadline():
        return 'Deadline'
    
    # Load flask form from data.
    flask_form = forms.create(form.questions)
    
    if flask_form.validate_on_submit():
        answers = []
        
        for question in form.questions:
            answers.append(getattr(flask_form, question['name']).data)
        
        form.leave_feedback(current_user, answers)
        
        return redirect(url_for('index'))
    
    return render_template('fill_form.html', form = flask_form)

@app.route('/forms/constructor', methods = ['GET', 'POST'])
@login_required
def form_constructor():
    course = get_element_by_id(models.Course)
    
    if course is None:
        return 'Cannot find the course.'
    
    if not course.check_permission(current_user, models.utils.Permission.SPEC):
        return 'No permission'
    
    form = forms.Creator()
    
    if form.validate_on_submit():
        name = form.name.data
        
        deadline  = form.deadline.data
        
        q = json.loads(form.json.data)
        
        course.create_form(current_user, name,  _deadline = deadline, questions = q)
        
        return redirect_next() 
    
    return render_template('form_constructor.html', form = form)

@app.route('/forms/delete')
@login_required
def form_delete():
    form = get_element_by_id(models.Form)
    
    if form is None:
        return 'Cannot find the form.'
    
    if not form.check_permission(current_user, models.utils.Permission.MAKE):
        return 'No permission'
    
    form.remove()

    flash('Succesfully removed.')

    return redirect_next()
    
@app.route('/forms/create', methods = ['GET', 'POST'])
@login_required
def create_form():
    FORMS = {'simple_form': [{
        "type": "RadioField", 
        "name": "rating", 
        "params": { "choices": ["1", "2", "3", "4"], 
         "label": "Rate the course"},
        "validators": [{ "type": "DataRequired" }]
    },
    {
        "type": "StringField", 
        "name": "comment", 
        "params": { "label": "Any suggestions or comments?" }
    }]}
    
    course = get_element_by_id(models.Course)
    
    if course is None:
        return 'Cannot find the course.'
    
    if not course.check_permission(current_user, models.utils.Permission.MAKE):
        return 'No permission'
    
    
    form = forms.Creator()
    
    if form.validate_on_submit():
        name = form.name.data
        
        deadline  = form.deadline.data
        
        q = json.load(open(f'app/test/Questions/{form.form.data}.json'))
        
        print(name, deadline, q)
        
        course.create_form(current_user, name,  _deadline = deadline, questions = q)
        
        return redirect_next()
    
    return render_template('fill_form.html', form = form)


@app.route("/courses/forms/worksheets")
@login_required
def view_worksheets():
    form = get_element_by_id(models.Form)
    
    if form is None:
        return 'Cannot find the form.'
    
    if not form.check_permission(current_user, models.utils.Permission.VIEW):
        return 'No permission'
    
    results = list()
    for feedback in form.feedbacks:
        results.append(feedback.get_feedback())
    return render_template("worksheets.html", name=form.name, results=results) 
    

@app.route("/courses/forms/answers")
@login_required
def view_answers():
    form = get_element_by_id(models.Form)
    
    if form is None:
        return 'Cannot find the form.'
    
    if not form.check_permission(current_user, models.utils.Permission.VIEW):
        return 'No permission'

    results = dict()
    for question in form.questions:
        type = question["type"]
        if type == "RadioField":
            results[question["params"]["label"]] = { "type" : type, "answers" : dict() }
        elif type == "StringField":
            results[question["params"]["label"]] = { "type" : type, "answers" : list() }
        

    for feedback in form.feedbacks:
        for (question, answer) in feedback.get_feedback():
            if results[question]["type"] == "RadioField":
                if answer not in results[question]["answers"]:
                    results[question]["answers"][answer] = 0
                results[question]["answers"][answer] += 1
            elif results[question]["type"] == "StringField":
                results[question]["answers"].append(answer)
    return render_template("answers.html", results=results)
