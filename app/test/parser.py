import re
import os
import glob
import json
import requests

from lxml import html as html_parser

def parse_roles(path):    
    users = {}
    
    for file_name in glob.glob(path):
        with open(file_name) as file:
            html = file.read()
            
        tree = html_parser.fromstring(html)
        
        print('PARSE:', file_name)
        
        course_id = int(os.path.splitext(os.path.split(file_name)[1])[0])
        
        for participant in tree.xpath('//table[@id="participants"]/tbody/tr[@class!="emptyrow"]'):
            user_id = int(re.findall(r'id=[0-9]+', participant.xpath('td[1]/a')[0].get('href'))[0][3:]) 
            
            role = participant.xpath('td[2]')[0].text
            
            u = users.get(user_id, {})
            
            u[course_id] = role
            
            users[user_id] = u
            
    return users
 
def parse_users_and_courses(path):
    users       = {}
    all_courses = {}
    
    for file_name in glob.glob(path):
        with open(file_name) as file:
            html = file.read()
        
        if 'The details of this user are not available to you' in html:
            print('Blocked:', file_name); continue
        
        elif 'This user account has been deleted' in html:
            print('Deleted:', file_name); continue
        
        else:
            print('Parsed:', file_name)
        
        tree = html_parser.fromstring(html)
        
        email = tree.xpath('//div[@class="profile_tree"]/section[1]/ul/li/dl/dd/a')
        
        if len(email) == 0:
            print('EMPTY EMAIL:', file_name); continue
        
        else:
            if len(email) != 1:
                print('SEVERAL EMAILS:', [i.text for i in email], file_name)
                
                index = input('index = ')
                
                if index:
                    email = email[int(index)].text
                else:
                    continue
                
                input(email)
            
            else:
                email = email[0].text
            
            if not re.match(r'[^@]+@[^@]+\.[^@]+', email):
                print('BAD EMAIL:', email, file_name); input(); continue

        courses = {}
        users
        for course in tree.xpath('//div[@class="profile_tree"]/section[3]/ul/li/dl/dd/ul/li/a'):
            id     = re.findall(r'course=[0-9]+', course.get('href'))[0]
            course = course.text.strip()
            
            if re.match(r'\[F20.+', course):
                courses[int(id[7:])] = course
                
            else:
                print('SKIP:', course)
        
        if len(course) == 0:
            print('ZERO COURSES', email, file_name); continue
        
        user_id = os.path.splitext(os.path.split(file_name)[1])[0]
        
        users[int(user_id)] = {'email': email, 'courses': list(courses.keys())}
        
        all_courses.update(courses)
    
    
    return (users, all_courses)
    
def download(url, key, ids, path):
    for i in ids:
        r = requests.get(url % i, cookies = {'MoodleSession': key})
    
        print('DOWNLOAD:', i)
        
        with open(path % i, 'w') as file:
            file.write(r.text)

'''
users, courses = parse('raw/*.html')

with open('users.json', 'w') as file:
    json.dump(users, file)
    
with open('courses.json', 'w') as file:
    json.dump(courses, file)

    
key = 'nus0cnr3jbdpjvucbisd937jjb'    
url = 'https://moodle.innopolis.university/user/index.php?id=%i&perpage=1000'

ids = [int(i) for i in users['iv.izmailov@innopolis.university'].keys()]
    
download(url, key, ids, 'courses/%i.html')


print(users['iv.izmailov@innopolis.university'])
'''

with open('users.json') as file:
    users = json.load(file)
    
with open('courses.json') as file:
    courses = json.load(file)

roles = parse_roles('courses/*.html')

 
bd_users   = {}
bd_courses = {}

for user_id in roles:
    if str(user_id) in users:
        bd_users[users[str(user_id)]['email']] = roles[user_id]
    
        for course_id in roles[user_id]:
            bd_courses[course_id] = courses[str(course_id)]
    else:
        print(user_id)
        
with open('bd_users.json', 'w') as file:
    json.dump(bd_users, file)
    
with open('bd_courses.json', 'w') as file:
    json.dump(bd_courses, file)

