import json

import app.models as models

from app import db

def fill_simple_data():
    db.session.add(models.Course(_name = 'OS'))
    db.session.add(models.Course(_name = 'P&S'))
    db.session.add(models.Course(_name = 'DE'))
    db.session.add(models.Course(_name = 'FSE'))
    db.session.commit()
    
    u = models.User()
    
    u.add_course(models.Course.query.get(1), models.utils.Permission.STUDENT)
    u.add_course(models.Course.query.get(2), models.utils.Permission.STUDENT)

    db.session.add(u)
    db.session.commit()
    
    
    u = models.User()
    
    u.add_course(models.Course.query.get(3), models.utils.Permission.STUDENT)
    u.add_course(models.Course.query.get(4), models.utils.Permission.STUDENT)

    db.session.add(u)
    db.session.commit()
    
    
    doe = models.User()
    
    doe.add_course(models.Course.query.get(1), models.utils.Permission.DOE)
    doe.add_course(models.Course.query.get(2), models.utils.Permission.DOE)
    doe.add_course(models.Course.query.get(3), models.utils.Permission.DOE)
    doe.add_course(models.Course.query.get(4), models.utils.Permission.DOE)
    
    db.session.add(doe)
    db.session.commit()
    
    
    prof = models.User()
    
    prof.add_course(models.Course.query.get(1), models.utils.Permission.PROFESSOR)
    prof.add_course(models.Course.query.get(2), models.utils.Permission.PROFESSOR)
    prof.add_course(models.Course.query.get(3), models.utils.Permission.PROFESSOR)
    prof.add_course(models.Course.query.get(4), models.utils.Permission.PROFESSOR)
    
    db.session.add(prof)
    db.session.commit()
    
    
    q = json.load(open('app/test/Questions/simple_form.json'))

    db.session.add(models.Form(_name = 'TestForm', _course_id = 1, questions = q))
    db.session.add(models.Form(_name = 'TestForm', _course_id = 3, questions = q))
    
    db.session.commit()
