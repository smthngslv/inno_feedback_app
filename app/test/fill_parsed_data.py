import os
import json


import app.models as models

from app import db


def fill(path):
    with open(os.path.join(path, 'bd_users.json')) as file:
        users = json.load(file)
        
    with open(os.path.join(path, 'bd_courses.json')) as file:
        courses = json.load(file)
    
    courses_map_id = {}
    
    for course_id in courses:
        c = models.Course(_name = courses[course_id])
        
        db.session.add(c)
        db.session.commit()
        
        courses_map_id[course_id] = c.id
        
        print(c.id, c.name)
        
    for (email, roles) in users.items():
        u = models.User(_email = email)
        
        db.session.add(u)
        db.session.commit()
        
        print(u.id, u.email)

        for (course_id, role) in roles.items():
            c = models.Course.query.get(courses_map_id[course_id])
            
            if role == 'Student':
                perm = models.utils.Permission.STUDENT
            elif role == 'Teacher':
                perm = models.utils.Permission.PROFESSOR
            else:
                print(email, c.name, role); continue
            
            u.add_course(c, perm)
            
        db.session.commit()
