from datetime import datetime

from flask_wtf          import FlaskForm

from wtforms            import SubmitField, StringField, DateTimeField, SelectField, HiddenField
from wtforms.validators import DataRequired, Optional

class Creator(FlaskForm):
    name   = StringField(validators=[DataRequired()])
    
    deadline = DateTimeField(format='%A, %B %d %Y, %H:%M', validators = [Optional(), ])
    
    json = HiddenField(validators=[DataRequired()])
    
    #form   = SelectField(choices = ['simple_form', 'general_form'])
    
    submit = SubmitField('Save') 
 
    
