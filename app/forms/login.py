from flask_wtf          import FlaskForm

from wtforms            import StringField, SubmitField
from wtforms.validators import DataRequired, Email

class Login(FlaskForm):
    email = StringField('Innopolis Email', validators = [Email()])
    
    submit = SubmitField('Sign In') 
 
