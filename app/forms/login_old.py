from flask_wtf          import FlaskForm

from wtforms            import IntegerField, SubmitField
from wtforms.validators import DataRequired

class LoginOld(FlaskForm):
    id     = IntegerField('User id', validators=[DataRequired()])
    submit = SubmitField('Sign In') 
 
