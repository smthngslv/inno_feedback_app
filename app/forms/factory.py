import wtforms

from flask_wtf import FlaskForm


def create(fields):
    class GeneratedForm(FlaskForm):
        pass
    
    for field in fields:
        cls = getattr(wtforms, field['type'])
        
        params     = field.get('params', {})
        validators = field.get('validators', [])
        
        for i in range(len(validators)):
            cls_v = getattr(wtforms.validators, validators[i]['type'])
            
            validators[i] = cls_v(**validators[i].get('params', {}))
        
        setattr(GeneratedForm, field['name'], cls(validators = validators, **params))
    
    setattr(GeneratedForm, 'submit', wtforms.SubmitField())
    
    return GeneratedForm()
