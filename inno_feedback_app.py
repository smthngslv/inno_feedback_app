from app        import app, db
from app.models import User, Course, Form, Feedback

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Course': Course, 'Form': Form, 'Feedback': Feedback}
