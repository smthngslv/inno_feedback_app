# Setting-Up
1. Clone the repository by running the following command, where `BRANCH_NAME` is the name of the banch to be cloned.
```
git clone https://gitlab.com/smthngslv/inno_feedback_app.git -b BRANCH_NAME` 
```

2. After above command will be created a directory `inno_feedback_app`.
```
cd inno_feedback_app
```

3. Copy the `.env.example` to get simple configuration file.
```
copy .env.example .env
```

4. You can set debug mode by modifying `.env` 
```
FLASK_DEBUG=True
```

5. Setup the virtual environment.
```
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```
